import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Usuarios } from '../domain/intefaces/usuarios.inteface';
import { UsuarioDTO } from '../domain/dto/usuario.dto';

@Injectable()
export class UsuarioCase {
  constructor(
    @InjectModel('Usuario') private readonly usuarioModel: Model<Usuarios>,
  ) {}

  async create(usuarioDTO: UsuarioDTO): Promise<Usuarios> {
    const usuario = new this.usuarioModel(usuarioDTO);

    return await usuario.save();
  }

  async all(): Promise<any> {
    const us = await this.usuarioModel.find();
    const usuario = await this.usuarioModel.find();

    const info = usuario.map(function (object) {
      const data = {
        rut: object.rut,
        nombre: object.nombre,
        celular: object.celular,
        email: object.email,
        banco: object.banco,
        tipo_cuenta: object.tipo_cuenta,
        nro_cuenta: object.nro_cuenta,
      };
      // console.log('effsfs', data);
      
      return data;
    });

    

    // return await Promise.all(info)
    //   .then((r) => {
    //     console.log(r);
    //     return info;
    //   })
    //   .catch((er) => {
    //     return er;
    //   });

    return info;
  }
}
