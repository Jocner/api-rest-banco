import * as mongoose from 'mongoose';


export const UsuarioSchema = new mongoose.Schema({
  rut: {
    type: String,
    required: true,
  },
  nombre: {
    type: String,
    required: true,
  },
  celular: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  banco: {
    type: String,
    required: true,
  },
  tipo_cuenta: {
    type: String,
    required: true,
  },
  nro_cuenta: {
    type: String,
    required: true,
  },
});




