import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length, Matches } from 'class-validator';

export class UsuarioDTO {
  @ApiProperty({
    description: 'The rut of the User',
    example: '26656565-3',
  })
  @IsNotEmpty()
  readonly rut: string;

  @ApiProperty({
    description: 'The name of the User',
    example: 'Jhon Doe',
  })
  @IsNotEmpty()
  readonly nombre: string;

  @ApiProperty({
    description: 'number celular',
    example: '+56945451212',
  })
  @IsNotEmpty()
  readonly celular: string;

  @ApiProperty({
    description: 'The email address of the User',
    example: 'jhon.doe@gmail.com',
  })
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @ApiProperty({
    description: 'The of bank',
    example: 'Banco Ripley',
  })
  @IsNotEmpty()
  readonly banco: string;

  @ApiProperty({
    description: 'type cuenta',
    example: 'Corriente',
  })
  @IsNotEmpty()
  readonly tipo_cuenta: string;

  @ApiProperty({
    description: 'number the cuenta',
    example: '4651652',
  })
  @IsNotEmpty()
  readonly nro_cuenta: string;

}


