import { Document } from 'mongoose';

export interface Usuarios extends Document {
  readonly rut: string;
  readonly nombre: string;
  readonly celular: string;
  readonly email: string;
  readonly banco: string;
  readonly tipo_cuenta: string;
  readonly nro_cuenta: string;
}
