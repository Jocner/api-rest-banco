import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';
import { UsuarioDTO } from '../domain/dto/usuario.dto';
import { UsuarioCase } from '../application/usuario.case';

@Controller('usuario')
export class UsuarioController {
  constructor(private usuarioCase: UsuarioCase) {}

  @Post('/')
  async createUsuario(@Res() res, @Body() usuarioDTO: UsuarioDTO) {
    const usuario = await this.usuarioCase.create(usuarioDTO);

    return res.status(HttpStatus.OK).json({
      msg: 'Usuario creado con exito!!!',
      usuario,
    });
  }

  @Get('/')
  async buscador(@Res() res): Promise<any> {
    const cliente = await this.usuarioCase.all();

    const data = cliente.forEach((element) => {
      return element;
    });

    return res.status(HttpStatus.OK).json(cliente);
  }
}
