import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsuarioSchema } from '../domain/models/usuario.schema'
import { UsuarioController } from '../infrastructure/usuario.controller';
import { UsuarioCase } from '../application/usuario.case';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Usuario', schema: UsuarioSchema }]),
  ],
  providers: [UsuarioCase],
  controllers: [UsuarioController],
})
export class OpertionsModule {}
