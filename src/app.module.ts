import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { OpertionsModule } from './opertions/opertions.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://jocner:jocner@cluster0.axw6l.mongodb.net/mean_banco?retryWrites=true&w=majority',
    ),
    OpertionsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
